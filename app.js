//By Derek Cardol - for To-Increase - GP 2015

var express = require("express");
var stylus = require('stylus');
var nib = require('nib');
var vogels = require('vogels');
var Joi = require('joi');
var moment = require('moment');
moment().format();

//Credentials
vogels.AWS.config.update({
  accessKeyId: process.env.AWS_ACCES_KEY_ID, 
  secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY, 
  region: process.env.AWS_REGION 
});

//de ekte baas
var piet;
var today;
var coffeeTimes = [];
var dataCount;

//define model for database
var SensorData = vogels.define('SensorData', {
  hashKey : 'sensor_id',
  rangeKey : 'date',
  schema : {
    sensor_id : Joi.string(),
    date      : Joi.date(),
    date_only : Joi.number()
  },
  indexes : [{
    hashKey : 'date_only', 
    name : 'date_only-index', 
    type : 'global'
  }]
});

//Config table
SensorData.config({tableName: 'Coffee'});

//make the current date to query
function makeDate(){
  var dd = moment().date();
  var mm = moment().month()+1; //month starts at zero
  var yyyy = moment().year();
  if(dd<10){
    dd='0'+dd;
  } 
  if(mm<10){
      mm='0'+mm;
  } 
  today = parseInt(yyyy + "" + mm + "" + dd);
  return today;
}

//Get only the time from datestring
function date2time(data) {
  dataCount = data.Count;
  for (var j = 0; j < dataCount; j++) {
    //console.log(data.Items[j].attrs.date);
    var parse_to_date = moment(data.Items[j].attrs.date, "YYYY-MM-DD HH:mm:ss");
    var parsedmin = parse_to_date.minute()
    if(parsedmin<10){
      parsedmin = '0' +parsedmin;
    }
    coffeeTimes[j] = parseInt(parse_to_date.hour() + "" + parsedmin);
    //console.log(coffeeTimes[j])
  }
  //sort ascending order
  coffeeTimes.sort(function(a, b) {
    return a - b;
  });
  return coffeeTimes;
}

//Map the directories http://www.clock.co.uk/blog/a-simple-website-in-nodejs-with-express-jade-and-stylus
var app = express();
function compile(str, path) {
  return stylus(str)
    .set('filename', path)
    .use(nib());
}
app.set('views', __dirname + '/views');
app.set('view engine', 'jade');
app.use(stylus.middleware(
  { src: __dirname + '/public'
  , compile: compile
  }
));
app.use(express.static(__dirname + '/public'));

//When the URL is called, render the webapp
app.get('/', function(req, res) {
  SensorData
    .query(makeDate()) //YYYYMMDD
    .usingIndex('date_only-index')
    .exec(function(err, data) {
      if (err) {
        console.log('error', err);
      }
      piet = date2time(data);
      res.render('layout.jade', {
        title: 'Home',
        piet: piet
      });
      console.log('date', makeDate());
    });
});

app.listen(8080);