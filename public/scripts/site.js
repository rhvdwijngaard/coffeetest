//By Derek Cardol - for To-Increase - GP 2015

//Global variables
var space, newPosition, totalCups, timeString;
var currentPosition = 0;
var edge = 20;
var cupStack = 0;
var coffeeTimeArray = [];

//makeCup(x position, y position)
function makeCup(a, b, c){
	var x = a;
	var y = b;
	var xsize = c;
	var ysize = xsize*1.5;
	var s = 0.6;
	var canvas = document.getElementById('myCanvas');
	var context = canvas.getContext('2d');

    context.beginPath();
    context.arc(x+(xsize*s), y+(ysize*0.4*s), (ysize/4*s), 1.5*Math.PI, 0.5*Math.PI);
    context.rect(x, y, xsize*s, ysize*s);
    context.fillStyle = 'white';
    context.fill();
    context.lineWidth = 3;
    context.strokeStyle = 'black';
    context.stroke();
}
//drawGraph(width, height) of canvas size
function drawGraph(w, h){
	var gWidth = w;
	var gHeight = h;
	var radius = 3;
	var bigDot = 0;
	var time = 8;
	
	var canvas = document.getElementById('myCanvas');
	var c = canvas.getContext('2d');
	
	//Vertical axis
	c.beginPath();
	c.moveTo(edge, edge);
	c.lineTo(edge, gHeight-edge*2);
	c.lineWidth = radius;
	c.strokeStyle = 'black';
	c.stroke();
	
	c.moveTo(edge, gHeight-edge*2);
	
	//Calculate horizontal axis length
	defineSpace(gWidth);
	
	//set the first time
	c.font="16px Arial";
	c.textAlign="center"; 
	c.fillText(time + ":00", newPosition, gHeight-(edge*0.5)); 
	time += 1;
	
	//Horizontal axis
	for(var i=0; i<40; i++){
		bigDot++;
		//Every 4th dot is a bigger one to mark an hour
		if(bigDot==4){
			bigDot=0;
			c.fillText(time + ":00", edge+newPosition, gHeight-(edge*0.5)); 
			c.arc(edge+newPosition, gHeight-edge*2, radius*1.5, 0, 2*Math.PI);
			time++;
			
		}
		else{
			c.arc(edge+newPosition, gHeight-edge*2, radius, 0, 2*Math.PI);
		}
		c.fill();
		c.stroke();
		c.lineTo(newPosition, gHeight-edge*2);
		c.lineWidth = radius;
		c.strokeStyle = 'black';
		c.stroke();
		newPosition = newPosition+space;
	}
}
//Divide line in 15min periods, 8:00 - 18:00
function defineSpace(w){
	space = (w-(edge*2))/40;
	newPosition = space;
}
//Calculate the height for the cup stack
function cupHeigth(){
	var ypos = document.getElementById("myCanvas").height-(edge*2)-(space*1.2)-(cupStack*space*1.2);
	return ypos;
}
//Place the cups according to time, and increase the cupstack
function placeCup(xposition){
	makeCup(xposition+(space/10), cupHeigth(), space);
	cupStack++;
}
//Define the x-position for every cup from the database according to their time
function definePos(){
	totalCups = 0 + coffeeTimeArray.length
	for(var j=0; j<totalCups; j++){
		var hours = Math.floor((coffeeTimeArray[j]-800)/100);
		var min = (coffeeTimeArray[j]-800)%100;
		//calculate position in pixels for the graph
		var realPosition = edge + (hours*(4*space)) + (Math.floor(min/15)*space);
		//check timeslot, if the cup is in the next 15min slot, reset cupstack
		if(realPosition != currentPosition){
			cupStack=0;
			placeCup(realPosition);
			currentPosition = realPosition;
		}
		else {
			placeCup(realPosition);
		}
	}
	//timestring to display last cup 
	if(timeString){
		timeString = "Nope";
	}
	else {
		timeString = (hours+8) + ":" + min;
	}
}

